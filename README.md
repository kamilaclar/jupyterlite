# JupyterLite Demo

JupyterLite deployed as a static site to GitLab Pages, for demo purposes.

This site is a clone of jupyterlite github pages demo <https://github.com/jupyterlite/demo> created by user Benabel

Update is made with the bash script `update.sh`.
